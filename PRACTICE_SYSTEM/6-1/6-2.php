<?php
    if (isset($_GET['dateToday'])) {
        $DateToday = $_GET['dateToday'] ?? '';
    } else {
        $DateToday = date('Y-m');
    }

    $time= strtotime($DateToday . '-01');

    if ($time === false) {
        $DateToday = date('Y-m');
        $time = strtotime($DateToday . '-01');
    }
    
    $today = date('Y-m-j', time());
     $varyDate = date('F Y', $time);

    $dateTime1 = date('Y-m');
    $dateTime2 = date($DateToday);


    $days = date('d', $time);
    $months = date('m', $time);
    $years = date('Y', $time);
    $today = strtotime ("now");
    
    $prev = date('Y-m', mktime(0, 0, 0, date('m', $time)-1, 1, date('Y', $time)));
    $next = date('Y-m', mktime(0, 0, 0, date('m', $time)+1, 1, date('Y', $time)));
    
    $firstDay = mktime(0,0,0,$months,1,$years);
    $daysInMonth = cal_days_in_month(0, $months, $years);
    $dayWeek = date('w', $firstDay);

    $currentDay = date('j');
 ?>
<!DOCTYPE html>
<html>
<head>
    <style>
        #today{
            background-color:pink;
        }
        ul {list-style-type: none;}
        body {font-family: sans-serif;}

        /* Month header */
        .month {
        padding: 70px 25px;
        width: 250px;
        background: lightblue;
        text-align: center;
        }

        /* Month list */
        .month ul {
        margin: 0;
        padding: 0;
        }

        .month ul li {
        color: white;
        font-size: 20px;
        text-transform: uppercase;
        letter-spacing: 3px;
        }


        .month .next {
        float: right;
        padding-top: 10px;
        }

        .month .prev {
        float: left;
        padding-top: 10px;
        }

        .weekdays {
        margin: 0;
        padding: 10px 0;
        background-color:#ddd;
        }

        .weekdays li {
        display: inline-block;
        width: 13.6%;
        color: #666;
        text-align: center;
        }

        .days li .active {
        padding: 5px;
        background: #1abc9c;
        color: white !important
        }

        .days {
        padding: 10px 0;
        background: #eee;
        margin: 0;
        }

        .days li {
        list-style-type: none;
        display: inline-block;
        width: 13.6%;
        text-align: center;
        margin-bottom: 5px;
        font-size:12px;
        color: #777;
        }



    </style>
    <meta charset="utf-8">
    <title>CALENDAR</title>
</head>
<body>
    <div class="month">
        <form action="" method="post">
            <h3></a> <?= $varyDate ?> </a></h3>
        </form>
        <table>
            <thead>
                <tr>
                    <th>Sun</th>
                    <th>Mon</th>
                    <th>Tue</th>
                    <th>Wed</th>
                    <th>Thu</th>
                    <th>Fri</th>
                    <th>Sat</th>
                </tr>
             </thead>
            <tbody>
                <tr>
                <?php
                    if(0 != $dayWeek) { 
                        echo('<td colspan="'.$dayWeek.'"> </td>'); 
                    }
                    for($i=1;$i<=$daysInMonth;$i++) {
                        if($i == $currentDay && $dateTime1 === $dateTime2  ) { 
                            echo('<td id="today">'); 
                        } else { 
                            echo("<td>"); 
                        }
                        echo"<div class='date'>".($i)."<div/>";
                        echo("</td>");
                        if(date('w', mktime(0,0,0,$months, $i, $years)) == 6) {
                            echo("</tr><tr>");
                        }
                    }
                ?>
                </tr>
            </tbody>
        </table>
        <a href="?date=<?= $prev ?>"><<       <a href="?date=<?= $next ?>">>>
    </div>
</body>
</html>