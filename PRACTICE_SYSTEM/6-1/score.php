<?php
    require_once 'connect.php'; 
    $answers = $_POST['answerchecked'] ?? '' ;
    $totalscore = 0;
    foreach ($answers as $key => $answer) {
        $sql = $dbc->prepare("SELECT answer.answer,question.question from answers  
        JOIN questions  ON question.questions_ID = answer.answer_ID WHERE answer = ?");
        $sql->bind_param('s',$answer);
        sql->execute();
        $results = $sql->get_result();
        while ($result = mysqli_fetch_array($results, MYSQLI_ASSOC)) {
            if($result['answer'] == $answer){
                $totalscore += 1 ;
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Results</title>
</head>
<body>
<div class="container">
    <h1>Result</h1>
        <?php if($score>=5){
           echo "<h2>Congratulations!</h2>";
        } else{
            echo "<h2>Sorry, you failed!</p></h2>"; 
        }
        ?>
    <div class="score">
        <h2><?php echo $score*10 ?>%</h2>
    </div>
 <div>
</body>
</html>