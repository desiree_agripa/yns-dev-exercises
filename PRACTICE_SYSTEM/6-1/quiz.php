<?php include_once 'connect.php';

$question_query = "SELECT * FROM questions ORDER BY RAND()";
$results = $dbc->query($question_query);

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <title>SHORT QUIZ</title>
  </head>
  <body>
    <div class="container">
    <div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-6">
	    <hr>
	    <center><label>SHORT QUIZ</label></center>
        <hr>
        <form action="score.php" method="post">
        <div class="content">
            <?php foreach ($results as $key => $resultdata):?>
                <p><?= $key+ 1, '.'. $resultdata['question'] ?></p>
                <?php for ($i=1;$i<4; $i++): ?>
                    <div>
                        <input type="radio" name="answercheckes[<?= $key ?>]" value="<?= $resultdata['choice'.$i] ?>">
                        <label for="<?= $resultdata['choice'.$i] ?>"><?= $resultdata['choice'.$i] ?></label><br>
                    </div>
                <?php endfor ; ?><br>
            <?php endforeach ;?>  
        </div>
        <center>
        <input class="btn btn-primary" name="Submit" type="submit" value="Submit">
        </center>
        </form>
    		</div>
    </div>
	</div>
  </body>
</html>

