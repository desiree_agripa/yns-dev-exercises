<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercise Navigation</title>
</head>
<body>
<div class='content'>
    <h1>Exercises in Html and Php</h1>
</div>
<div>
    <ul>
        <li><a href="./HTML&PHP/1-1.php">1-1: Show Hello World</a></li>
        <li><a href="./HTML&PHP/1-2.php">1-2: The four basic operation</a></li>
        <li><a href="./HTML&PHP/1-3.php">1-3: Show greatest common divisor</a></li>
        <li><a href="./HTML&PHP/1-4.php">1-4: Solve the fizzbuzz</a></li>
        <li><a href="./HTML&PHP/1-5.php">1-5: Input data</a></li>
        <li><a href="./HTML&PHP/1-6.php">1-6 to 1-9 and 1-13</a></li>
        <li><a href="./HTML&PHP/1-10.php">1-10 to 1-12</a></li>
    </ul>   
</div>

<div class='content'>
    <h1>Exercises Javascript</h1>
</div>
<div>
    <ul>
        <li><a href="./JAVASCRIPT/2-1.html">2-1: Show alert</a></li>
        <li"><a href="./JAVASCRIPT/2-2.html">2-2: Confirm dialog and redirection</a></li>
        <li><a href="./JAVASCRIPT/2-3.html">2-3: The four basic operation</a></li>
        <li><a href="./JAVASCRIPT/2-4.html">2-4: Show prime numbers</a></li>
        <li><a href="./JAVASCRIPT/2-5.html">2-5: Show in the label</a></li>
        <li><a href="./JAVASCRIPT/2-6.html">2-6: Press the button</a></li>
        <li><a href="./JAVASCRIPT/2-7.html">2-7: Show alert when click image</a></li>
        <li><a href="./JAVASCRIPT/2-8.html">2-8: Show alert when click link</a></li>
        <li><a href="./JAVASCRIPT/2-9.html">2-9: Change text background</a></li>
        <li><a href="./JAVASCRIPT/2-10.html">2-10: Scroll screen when press buttons</a></li>
        <li><a href="./JAVASCRIPT/2-11.html">2-11: Change background using animation</a></li>
        <li><a href="./JAVASCRIPT/2-12.html">2-12: Show another image mouse hover</a></li>
        <li><a href="./JAVASCRIPT/2-13.html">2-13: Change the size of images</a></li>
        <li><a href="./JAVASCRIPT/2-14.html">2-14: Show images according to the option</a></li>
        <li><a href="./JAVASCRIPT/2-15.html">2-15: Show current data and time in real time</a></li>
    </ul>   
</div>
<div class='content'>
    <h1>Exercises Database</h1>
</div>
<div>
    <ul>
        <li><a href="./DATABASE/3-2 to 3-3.sql">3-2 to 3-3 create, insert, update, delete query</a></li>
        <li><a href="./DATBASE/3-4.sql">3-4 Sql problems</a></li>
        <li><a href="./DATABASE/3-5/1-10(1).php">3-5</a></li>
    </ul>
    <h1>Advance Database</h1>
    <ul>
        <li><a href="./Advance Database/4.sql">Advance Sql</a></li>
    </ul>   
</div>
<div class='content'>
    <h1>Practice System/Programs</h1>
</div>
<div>
    <ul>
        <li><a href="./PRACTICE_SYSTEM/6-1/quiz.php">Quiz with three multiple choices</a></li>
    </ul>
    <ul>
        <li>
            <a href="./PRACTICE_SYSTEM/6-2.php">Calendar</a>
    </ul>
</div>
</body>
</html>