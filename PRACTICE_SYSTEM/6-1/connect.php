<?php 
/*
    Data base connection
*/
/*
Define Constants
*/

define('DB_NAME', 'quiz');
define('DB_USER', 'root');
define('DB_PASSWORD', '');
define('DB_HOST', 'localhost');


@$dbc=mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);
if(!$dbc){
    echo mysqli_connect_error();
    exit();
}
mysqli_set_charset($dbc,"utf8");

?>
