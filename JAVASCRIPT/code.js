//2-4 Prime Number
function primeNumber() {
  $number = document.getElementById('number').value
  if ($number < 1)
    document.getElementById('result').innerHTML = 'Negative Numbers is not prime number. Please enter a positive number'
  else {
    $primeNumbers = [];
    for (var i = 2; i <= $number; i++) {
      $prime = 1;
      for (var j = 2; j < i; j++) {
        if (i % j == 0) {
          $prime = 0;
          break;
        }
      }
      if ($prime == 1) {
        $primeNumbers.push(i)
      }
    }     
    document.getElementById('result').innerHTML = 'Prime Numbers: ' + $primeNumbers
  }
}

//2-5  Input characters in the textbox and show it in the label.
function submit() {
  document.getElementById('outputData').innerHTML = document.getElementById('inputData').value
}

//2-6 Press the button and add a label below the button.
$("#label").on("click", function(){
  $(this).closest("div").append("<br><label>This is a label from a button.</label>");
});

//2-8 URL alert
function URLnotification() {
  alert(window.location.href)
}

//2-9 Button change background color
function changeColor(color) {
      document.getElementById("pink").style.backgroundColor = color.innerHTML;
      document.getElementById("red").style.backgroundColor = color.innerHTML;
      document.getElementById("blue").style.backgroundColor = color.innerHTML;
      document.getElementById("green").style.backgroundColor = color.innerHTML;
    }

//2-10 scroll up and down page
function scrollDown() {
  document.getElementById("scrollup").scrollIntoView();
  }
  function scrollUp() {
  document.getElementById("scrolldown").scrollIntoView();
  }

  //2-11 Change Background color
  function changeBackgroundColor() {
    let bgcolors = ['darkblue','lightblue'];
    let newColor  = 0;
    setInterval(function(){
        document.querySelector('body').style.background = bgcolors[newColor];
            newColor++;
        if (newColor == bgcolors.length) newColor = 0;
    }, 3000);
  }
