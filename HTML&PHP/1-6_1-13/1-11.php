<?php 
$csv_file = "user_information_data.csv";
 $open_data = fopen($csv_file, "r");
 $get_data = fgetcsv($open_data, 1000, ",");
 $all_data = [];
 while(($get_data = fgetcsv($open_data, 1000, ',')) != FALSE){
 	$all_data[] = $get_data;

 }
 fclose($open_data);
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <title>Display </title>
  </head>
  <body>
    <div class="container">
    <div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-6">
	    <hr>
	    <center><label>USER INFORMATION</label></center> <hr>
		    	<form action="" method="POST" role="form" enctype="multipart/form-data">
				<div class="panel panel-default panel-body col-sm-12">
					<table class="table table-condensed table-bordered text-center">
						<thead>
							<th>Firstname</th>
							<th>Middlename</th>
							<th>Lastname</th>
							<th>Contact Number</th>
							<th>Email Address</th>
							<th>Photo</th>
						</thead>
						<tbody>
							<?php
									foreach ($all_data as $data) { ?>
											<tr>
											<td><?= $data[1]; ?></td>
											<td><?= $data[2]; ?></td>
											<td><?= $data[3]; ?></td>
											<td><?= $data[4]; ?></td>
											<td><?= $data[5]; ?></td>
											<td><img src="upload/<?= $data[6]; ?>" width="80px" height="70px"></td>
										</tr>
									<?php }

							 ?>
									</tbody>
								</table>
							</div>
						</form>
    		</div>
    	</div>
		</div>
  </body>
</html>

	
