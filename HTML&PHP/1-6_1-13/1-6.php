<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <title>User Information</title>
  </head>
  <body>
    <div class="container">
    <div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-6">
	    <hr>
	    <center><label>USER INFORMATION</label></center>
		    	<form action="1-6_output.php" method="POST" role="form" enctype="multipart/form-data">
				<div class="panel panel-default panel-body col-sm-12">
					<div class="form-group hidden">
						<label for="">Firstname</label>
						<input type="text" class="form-control" id="" placeholder="Firstname" name="fname" value="">
					</div>
					<div class="form-group hidden">
						<label for="">Middlename</label>
						<input type="text" class="form-control" id="" placeholder="Middlename" name="mname" value="">
					</div>
					<div class="form-group hidden">
						<label for="">Lastname</label>
						<input type="text" class="form-control" id="" placeholder="Lastname" name="lname" value="">
					</div>
					<div class="form-group hidden">
						<label for="">Contact Number</label>
						<input type="text" class="form-control" id="" placeholder="Contact Number" name="contact" value="">
					</div>
					<div class="form-group hidden">
						<label for="">Address</label>
						<input type="text" class="form-control" id="" placeholder="Address" name="address" value="">
					</div><br>
					<div class="form-group hidden">
						<center><button class="btn btn-primary" name="update_user">Submit</button></center>
					</div>
				</div>
			</form>
    		</div>
        </div>
	</div> 
  </body>
</html>
