<?php 
  session_start();

   if (isset($_SESSION['user_ID'])) {
      echo "<script>window.location.href = '1-10.php';</script>";
    }

  $userID=$password=$userID_error=$password_error="";
  if(isset($_POST['login'])){

  	$user_ID = $_POST["user_ID"];
  	$password = $_POST["password"];

			if(file_exists("user_information_data.csv")){
				$csv_file = "user_information_data.csv";
						 $file = fopen($csv_file, "r");
						  while(($get_data = fgetcsv($file)) != FALSE){
							 			 if($user_ID == $get_data[7] && $password == $get_data[8]){
							 			 	$_SESSION['user_ID'] = $user_ID;
							 			 	echo "<script>window.location.href = '1-10.php';</script>";
					 			 }
					 		}
			 		 fclose($file);
			 		 $error = "Wrong User ID or Password";
			 		 
			}
  }

  //sanitize input data
	function input_data($data){
		$data = trim($data);
		$data = htmlspecialchars($data);
		$data = stripcslashes($data);
		return $data;
	}
	$fname=$mname=$lname=$contact=$email=$mname=$file="";
	$fname_error=$email_error=$contact_error=$lname_error=$mname_error=$file_error="";

	if($_SERVER["REQUEST_METHOD"]=="POST"){

		//uploading profile photo
		if(!empty($file)){
		//Profile photo
		$fileName = $_FILES["file"]["name"];
		$fileTmpName = $_FILES['file']['tmp_name'];
		$fileSize = $_FILES['file']['size'];
		$fileError = $_FILES['file']['error'];
		$fileType = $_FILES['file']['type'];

		$fileExt = explode('.',$fileName);
		$fileActualExt = strtolower(end($fileExt));

		$allowed = array('jpg','jpeg','png');
    
		if(in_array($fileActualExt, $allowed))
			{
				if($fileError === 0){
					if($fileSize < 5000000){
						$newFilename = uniqid('',true).".".$fileActualExt;
						 $profilePhoto = 'upload/'.$newFilename;
						 move_uploaded_file($fileTmpName, $profilePhoto);
					}else{
						$file_error="Error!";
					}
				}else{
					$file_error="Error!";
				}

			}else{
				$file_error = "You cannot upload file of this type";
			}
			}
		//Firstname
		if(empty($_POST["fname"])){
			$fname_error = "Firstname is required.";
		}else{
			$fname = input_data($_POST['fname']);
			//validate inputted firstname
			if(!preg_match("/^[a-zA-z]*$/", $fname)){
				$fname_error = "Firstname is not in valid format, can only contain letters.";
			}
		}
		//Middlename
		if(!empty($_POST["mname"])){
			$mname = input_data($_POST['mname']);
			if(!preg_match("/^[a-zA-z]*$/", $mname)){
				$mname_error = "Middlename is not in valid format, can only contain letters.";
			}
		}
		//Lastname
		if(empty($_POST["lname"])){
			$lname_error = "Lastname is required.";
		}else{
			$lname = input_data($_POST['lname']);
			if(!preg_match("/^[a-zA-z]*$/", $lname)){
				$lname_error = "Lastname is not in valid format, can only contain letters.";
			}
		}
		//Contact Number
		if(empty($_POST["contact"])){
			$contact_error = "Contact number is required.";
		}else{
			$contact = input_data($_POST['contact']);
			if(!preg_match('/^[0-9]{11}+$/', $contact)){
				$contact_error = "Contact number format is not valid.";
			}
		}
		//Email Address
		if(empty($_POST["email"])){
			$email_error = "Email Address is required.";
		}else{
			$email = input_data($_POST['email']);
			if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
				$email_error = "Email Address format is not valid.";
			}
		}

		if($fname_error=="" AND $mname_error=="" AND $lname_error=="" AND $email_error=="" AND $contact_error==""){
			$file_open = fopen("user_information_data.csv", "a");
			$no_rows = count(file("user_information_data.csv"));
			if($no_rows > 1){
				$no_rows = ($no_rows - 1) + 1;
			}else{
				$no_rows = $no_rows + 1;
			}
			$form_data = array(
					'number' => $no_rows,
					'fname' => $fname,
					'mname' => $mname,
					'lname' => $lname,
					'contact_number' => $contact,
					'email_address' => $email,
					'photo' => $newFilename
			);
			fputcsv($file_open, $form_data);
			$fname ='';
			$mname ='';
			$lname='';
			$contact='';
			$email="";

			echo '<script>alert("Submit Successful")</script>';
		}
	}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <title>Login</title>
  </head>
  <body>
    <div class="container">
    <div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-6">
	    <hr>
	    <center><label>LOGIN</label></center>
	    <div class="alert text-warning">
          <?php if (isset($error)): ?>
            <?php echo $error ?>
          <?php endif; ?>
        </div>
		    	<form action="" method="POST" role="form" enctype="multipart/form-data">
				<div class="panel panel-default panel-body col-sm-12">
					<div class="form-group hidden">
						<label for="">User ID</label>
						<input type="text" class="form-control" id="" placeholder="User ID" name="user_ID">
						<span style="color:red"><?= $userID_error; ?></span>
					</div>
					<div class="form-group hidden">
						<label for="">Password</label>
						<input type="password" class="form-control" id="" placeholder="Password" name="password">
						<span style="color:red"><?= $password_error; ?></span>
					</div>
					<div class="form-group hidden"><br>
						<center><button class="btn btn-primary" name="login">Login</button></center>
					</div>
				</div>
			</form>
    		</div>
    </div>
	</div>
  </body>
</html>