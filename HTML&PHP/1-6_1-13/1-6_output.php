<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <title>User Information</title>
  </head>
  <body>
    <div class="container">
    <div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-6">
    <hr>
    <center><label>USER INFORMATION</label></center>
	    <div class="center">
	    	<form action="" method="POST" role="form" enctype="multipart/form-data">
			<div class="panel panel-default panel-body col-sm-12">
				<div class="form-group hidden">
					<label for="">Firstname: </label><b> <?= strtoupper($_POST['fname']); ?></b>
				</div>
				<div class="form-group hidden">
					<label for="">Middlename: </label> <b><?= strtoupper($_POST['mname']); ?></b>
				</div>
				<div class="form-group hidden">
					<label for="">Lastname: </label><b> <?= strtoupper($_POST['lname']); ?></b>
				</div>
				<div class="form-group hidden">
					<label for="">Contact Number: </label><b> <?= strtoupper($_POST['contact']); ?></b>
				</div>
				<div class="form-group hidden">
					<label for="">Address: </label><b> <?= strtoupper($_POST['address']); ?></b>
				</div>
				<hr>		
			</div>
		</form>
		</div>
        </div>
    </div>
	</div>
  </body>
</html>
