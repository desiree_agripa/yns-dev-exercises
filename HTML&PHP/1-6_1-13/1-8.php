<?php 
	function input_data($data){
		$data = trim($data);
		$data = htmlspecialchars($data);
		$data = stripcslashes($data);
		return $data;
	}
	$fname=$mname=$lname=$contact=$email=$mname="";
	$fname_error=$email_error=$contact_error=$lname_error=$mname_error="";

	if($_SERVER["REQUEST_METHOD"]=="POST"){
		//Firstname
		if(empty($_POST["fname"])){
			$fname_error = "Firstname is required.";
		}else{
			$fname = input_data($_POST['fname']);
			//validate inputted firstname
			if(!preg_match("/^[a-zA-z]*$/", $fname)){
				$fname_error = "Firstname is not in valid format, can only contain letters.";
			}
		}
		//Middlename
		if(!empty($_POST["mname"])){
			$mname = input_data($_POST['mname']);
			if(!preg_match("/^[a-zA-z]*$/", $mname)){
				$mname_error = "Middlename is not in valid format, can only contain letters.";
			}
		}
		//Lastname
		if(empty($_POST["lname"])){
			$lname_error = "Lastname is required.";
		}else{
			$lname = input_data($_POST['lname']);
			if(!preg_match("/^[a-zA-z]*$/", $lname)){
				$lname_error = "Lastname is not in valid format, can only contain letters.";
			}
		}
		//Contact Number
		if(empty($_POST["contact"])){
			$contact_error = "Contact number is required.";
		}else{
			$contact = input_data($_POST['contact']);
			if(!preg_match('/^[0-9]{11}+$/', $contact)){
				$contact_error = "Contact number format is not valid.";
			}
		}
		//Email Address
		if(empty($_POST["email"])){
			$email_error = "Email Address is required.";
		}else{
			$email = input_data($_POST['email']);
			if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
				$email_error = "Email Address format is not valid.";
			}
		}
        //Creation and writing data in a csv file
		if($fname_error=="" AND $mname_error=="" AND $lname_error=="" AND $email_error=="" AND $contact_error==""){
			$file_open = fopen("user_information_data.csv", "a");
			$no_rows = count(file("user_information_data.csv"));
			if($no_rows > 1){
				$no_rows = ($no_rows - 1) + 1;
			}else{
				$no_rows = $no_rows + 1;
			}
			$form_data = array(
					'number' => $no_rows,
					'fname' => $fname,
					'mname' => $mname,
					'lname' => $lname,
					'contact_number' => $contact,
					'email_address' => $email
			);
			fputcsv($file_open, $form_data);
			$fname ='';
			$mname ='';
			$lname='';
			$contact='';
			$email="";
		}
	}
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <title>Form Validation</title>
  </head>
  <body>
    <div class="container">
    <div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-6">
	    <hr>
	    <center><label>Writing in csv file</label></center>
		    	<form action="" method="POST" role="form" enctype="multipart/form-data">
				<div class="panel panel-default panel-body col-sm-12">
					<div class="form-group hidden">
						<label for="">Firstname</label>
						<input type="text" class="form-control" id="" placeholder="Firstname" name="fname" value="<?= $fname; ?>">
						<span style="color:red"><?= $fname_error; ?></span>
					</div>
					<div class="form-group hidden">
						<label for="">Middlename</label>
						<input type="text" class="form-control" id="" placeholder="Middlename" name="mname" value="<?= $mname; ?>">
					</div>
					<div class="form-group hidden">
						<label for="">Lastname</label>
						<input type="text" class="form-control" id="" placeholder="Lastname" name="lname" value="<?= $lname; ?>">
						<span style="color:red"><?= $lname_error; ?></span>
					</div>
					<div class="form-group hidden">
						<label for="">Contact Number</label>
						<input type="text" class="form-control" id="" placeholder="Contact Number" name="contact" value="<?= $contact; ?>">
						<span style="color:red"><?= $contact_error; ?></span>
					</div>
					<div class="form-group hidden">
						<label for="">Email Address</label>
						<input type="text" class="form-control" id="" placeholder="Address" name="email" value="<?= $email; ?>">
						<span style="color:red"><?= $email_error; ?></span>
					</div><br>
					<div class="form-group hidden">
						<center><button class="btn btn-primary" name="">Submit</button></center>
					</div>
				</div>
			</form>
    		</div>
    </div>
	</div>
  </body>
</html>