<?php 
$all_data=$data_in_page=[];
if(file_exists("user_information_data.csv")){
	$csv_file = "user_information_data.csv";
	 $file = fopen($csv_file, "r");
	  while(($get_data = fgetcsv($file)) != FALSE){
 			 array_push($all_data, $get_data);
 		}
 		 fclose($file);
}
//Getting pagination number
if(!isset($_GET['page'])){
	$page = 1;
}else{
	$page = $_GET['page'];
}
	
	$data_info_csv = count($all_data);
	$number_pages = ceil($data_info_csv/10);
	$data_in_page = array_slice($all_data, ($page - 1) * 10, 10);
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <title>Display </title>
  </head>
  <body>
    <div class="container">
    <div class="row">
    <div class="col-sm-2"></div>
    <div class="col-sm-9">
	    <hr>
	    <center><label>USER INFORMATION</label></center> <hr>
		    	<form action="" method="POST" role="form" enctype="multipart/form-data">
				<div class="panel panel-default panel-body col-sm-12">
					<table class="table table-condensed table-bordered text-center">
						<thead>
							<th>No.</th>
							<th>Firstname</th>
							<th>Middlename</th>
							<th>Lastname</th>
							<th>Contact Number</th>
							<th>Email Address</th>
							<th>Photo</th>
						</thead>
						<tbody>
							<?php
								if(isset($data_in_page)){
								
									$count=1;
									foreach ($data_in_page as $key => $data) { 

										?>
											<tr>
											<td><?= $count; ?></td>
											<td><?= $data[1]; ?></td>
											<td><?= $data[2]; ?></td>
											<td><?= $data[3]; ?></td>
											<td><?= $data[4]; ?></td>
											<td><?= $data[5]; ?></td>
											<td><img src="upload/<?= $data[6]; ?>" width="80px" height="70px"></td>
										</tr>
									<?php 
										$count++;}
									} ?>
									</tbody>
								</table>
							</div>
								<div class="">
									<label for=""></label><center>
									<?php
											for($pages=1;$pages<=$number_pages;$pages++){ ?>
												<a href="1-12.php?page=<?= $pages; ?>"><?= $pages; ?></a>
													<?php	} ?>
										</center>
								</div>
						</form>
    		</div>
    	</div>
		</div>
  </body>
</html>

	
