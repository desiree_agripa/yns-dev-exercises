<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Display Date</title>

</head>
<body>
	<form method="POST">
	<label>Enter a Date: </label>
	<input type="date" name="date" value="" placeholder="Enter the a date">
	<input type="submit" name="Submit" value="Submit">
	<br><hr>
	</form>
	<br> 
<?php 
	if(isset($_POST["Submit"])){

		$date = $_POST['date'];

		$originalDate = date("F d, Y", strtotime($date));
		$addedDate = date("F d, Y", strtotime($date.' + 3 days'));

		while ($originalDate <= $addedDate) {
			$originalDate = date("F d, Y", strtotime($originalDate.' + 1 days'));
			echo $originalDate."-".date("l", strtotime($originalDate))."<br>";
		}
	}
 ?>
</body> 
</html>