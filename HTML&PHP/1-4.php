<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Solve the FizzBuzz problem.</title>

</head>
<body>
	<form method="POST">
	<label>Enter a Number: </label>
	<input type="textfield" name="number" value="" placeholder="Enter the and number">
	<input type="submit" name="Submit" value="Submit">
	<br><hr>
	</form>
	<br> 
	<?php 
	if(isset($_POST["Submit"])){

		$number = $_POST['number'];

		for($count=1; $count<=$number; $count++ ) {
		    if( ($count%3) === 0 && ($count%5) === 0 ) {
		        echo "FizzBuzz";
		        echo "<br>";
		    }else if( ($count%3) === 0 ) {
		        echo "Fizz";
		        echo "<br>";
		    }else if( ($count%5) === 0 ) {
		        echo "Buzz";
		        echo "<br>";
		    }else{
		        echo $count;
		         echo "<br>"; 
		    }
		}
	}

 ?>
</body>
</html>