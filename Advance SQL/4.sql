--4-1 Selecting by age using birth date
        SELECT first_name,middle_name last_name, birth_date, TIMESTAMPDIFF(YEAR, birth_date, CURDATE()) AS age FROM employees 
        WHERE TIMESTAMPDIFF(YEAR, birth_date, CURDATE()) > 30 and TIMESTAMPDIFF(YEAR, birth_date, CURDATE()) < 40;

--4-2 List all therapists according to their daily work shifts
        --A - Create table for therapist and daily_work_shift
        CREATE TABLE IF NOT EXISTS therapists(id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,name VARCHAR(100) NOT NULL);
        CREATE TABLE IF NOT EXISTS daily_work_shifts(id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, therapist_id int not null,
        target_date date not null,start_time time not null,end_time time not null);

        --B - Insert daily_work_shifts schedule
        INSERT INTO therapists VALUES('','John'),('','Arnold'),('','Robert'),('','Ervin'),('','Smith');
        INSERT INTO daily_work_shifts VALUES
        ('',1,NOW(),'14:00:00','15:00:00'),
        ('',2,NOW(),'22:00:00','23:00:00'),
        ('',3,NOW(),'00:00:00','01:00:00'),
        ('',4,NOW(),'5:00:00','5:30:00'),
        ('',1,NOW(),'21:00:00','21:45:00'),
        ('',5,NOW(),'5:30:00','5:50:00'),
        ('',3,NOW(),'2:00:00','2:30:00');

        --C Select mst_therapist_id, target_date, start_time, end_time and sort_start_time from daily_work_shifts using inner join to therapists order by target_date ASC and sort_start_time ASC
        SELECT therapist_id,target_date,start_time,end_time, CASE WHEN start_time <= '05:59:59' AND start_time >= ' 00:00:00' THEN
        CONCAT(target_date + interval 1 day, ' ' , start_time) ELSE CONCAT(target_date , ' ' , start_time) END sort_start_time FROM
        daily_work_shifts INNER JOIN therapists on therapists.id = therapist_id ORDER BY target_date ASC, sort_start_time ASC;

--4-3 Using Case conditional statement
        SELECT * , CASE WHEN positions.name = "CEO" THEN "Chief Executive Officer" 
        WHEN positions.name = "CTO"  THEN "Chief Technical Officer" 
        WHEN positions.name = "CFO" THEN "Chief Financial Officer" 
        ELSE positions.name END AS pos_name 
        FROM employee_positions INNER JOIN employees ON employees.id = employee_positions.employee_id 
        INNER JOIN positions ON positions.id = employee_positions.position_id; 

--4-4 Create sql that will display the OUTPUT

        SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
        START TRANSACTION;
        SET time_zone = "+00:00";

        --
        -- Database: `massage_company`
        --

        CREATE TABLE `daily_work_shifts` (
        `id` int(11) NOT NULL,
        `therapist_id` int(11) NOT NULL,
        `target_date` date NOT NULL,
        `start_time` time NOT NULL,
        `end_time` time NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

        INSERT INTO `daily_work_shifts` (`id`, `therapist_id`, `target_date`, `start_time`, `end_time`) VALUES
        (1, 1, '2022-01-19', '14:00:00', '15:00:00'),
        (2, 2, '2022-01-19', '22:00:00', '23:00:00'),
        (3, 3, '2022-01-19', '00:00:00', '01:00:00'),
        (4, 4, '2022-01-19', '05:00:00', '05:30:00'),
        (5, 1, '2022-01-19', '21:00:00', '21:45:00'),
        (6, 5, '2022-01-19', '05:30:00', '05:50:00'),
        (7, 3, '2022-01-19', '02:00:00', '02:30:00');

        CREATE TABLE `therapists` (
        `id` int(11) NOT NULL,
        `name` varchar(100) NOT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

        INSERT INTO `therapists` (`id`, `name`) VALUES
        (1, 'John'),
        (2, 'Arnold'),
        (3, 'Robert'),
        (4, 'Ervin'),
        (5, 'Smith');

        ALTER TABLE `daily_work_shifts`
        ADD PRIMARY KEY (`id`);

        ALTER TABLE `therapists`
        ADD PRIMARY KEY (`id`);


        ALTER TABLE `daily_work_shifts`
        MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;


        ALTER TABLE `therapists`
        MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
        COMMIT;


  
    