-- Employee table
CREATE TABLE IF NOT EXISTS employees(
id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
first_name		 VARCHAR(50) NOT NULL,
last_name		 VARCHAR(50) NOT NULL,
middle_name		 VARCHAR(50) NULL,
birth_date		 DATE NOT NULL,
department_id		 INT NOT NULL,
hire_date		 DATE  NULL,
boss_id		 INT  NULL,
CONSTRAINT emp_fk_dept_id FOREIGN KEY (department_id) REFERENCES departments (id)
);

-- Department Table
CREATE TABLE IF NOT EXISTS departments(
id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
name VARCHAR(50) NOT NULL
);

-- Postion Table
CREATE TABLE IF NOT EXISTS positions(
id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
name VARCHAR(50) NOT NULL
);

-- Employee table
CREATE TABLE IF NOT EXISTS employee_positions(
id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
employee_id		 INT NOT NULL,
position_id		 INT  NOT NULL,
CONSTRAINT emp_fk_emp_id FOREIGN KEY (employee_id) REFERENCES employees (id),
CONSTRAINT emp_fk_pos_id  FOREIGN KEY (position_id) REFERENCES positions (id)
);


ALTER TABLE employees
ADD CONSTRAINT emp_fk_boss_id FOREIGN KEY (boss_id) REFERENCES employees (id);

-- Insert data
INSERT INTO departments VALUES('','Executive'),('','Admin'),('','Sales'),('','Development'),('','Design'),('','Marketing');

INSERT INTO positions VALUES('','CEO'),('','CTO'),('','CFO'),('','Manager'),('','Staff');

INSERT INTO employees VALUES('','Manabu','Yamazaki',null,'1976-03-15','1',null,null),
('','Tomohiko','Takasago',null,'1974-05-24','3','2014-04-01','1'),
('','Yuta','Kawakami',null,'1990-08-13','4','2014-04-01','1'),
('','Shogo','Kubota',null,'1985-01-31','4','2014-12-01','1'),
('','Lorraine','San Jose','P.','1983-10-11','2','2015-03-10','1'),
('','Haille','Dela Cruz','A.','1990-11-12','3','2015-02-15','2'),
('','Godfrey','Sarmenta','L.','1993-09-13','4','2015-01-01','1'),
('','Alex','Amistad','F.','1988-04-14','4','2015-04-10','1'),
('','Hideshi','Ogoshi',null,'1983-07-15','4','2014-06-01','1'),
('','Kim',' ',' ','1977-10-16','5','2015-08-06','1');


INSERT INTO employee_positions VALUES('','1','1'),
('','1','2'),
('','1','3'),
('','2','4'),
('','3','5'),
('','4','5'),
('','5','5'),
('','6','5'),
('','7','5'),
('','8','5'),
('','9','5'),
('','10','5');		

--  SQL problems scripts

-- Number 1
select * from employees WHERE last_name LIKE 'k%';

-- Number 2
select * from employees WHERE last_name LIKE '%i';

-- Number 3
select concat(first_name,' ',middle_name,' ',last_name) full_name,hire_date from employees WHERE hire_date BETWEEN '2015/1/1' AND '2015/3/31' ORDER BY hire_date ASC;

-- Number 4
select e.last_name Employee,b.last_name Boss from employees e LEFT JOIN employees b on e.boss_id = b.id WHERE e.boss_id IS NOT NULL;

-- Number 5
SELECT last_name FROM employees LEFT JOIN departments ON department_id = departments.idWHERE departments.name = "Sales" ORDER BY last_name desc;

-- Number 6
SELECT COUNT(*) count_has_middle FROM employees WHERE middle_name is not null AND middle_name != '';

-- Number 7
SELECT departments.name,count(employees.id) from departments JOIN employees on departments.id = department_id GROUP BY department_id;

-- Number 8
SELECT first_name,middle_name,last_name,hire_date from employees ORDER BY hire_date DESC LIMIT 1;

-- Number 9
SELECT name,id from departments d WHERE not  EXISTS (SELECT department_id from employees e WHERE d.id = department_id);

-- Number 10
SELECT first_name,middle_name,last_name from employees e WHERE e.id in (SELECT employee_id FROM employee_positions GROUP BY employee_id HAVING COUNT(*) > 2);
