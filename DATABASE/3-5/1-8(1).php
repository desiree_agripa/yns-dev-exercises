<?php include_once 'connect.php';

$fname_error=$email_error=$contact_error=$lname_error=$mname_error="";
if($_SERVER["REQUEST_METHOD"]=="POST"){
    function input_data($data){
		$data = trim($data);
		$data = htmlspecialchars($data);
		$data = stripcslashes($data);
		return $data;
	}
	$fname=$mname=$lname=$contact=$email=$mname=$username=$password="";
	$fname_error=$email_error=$contact_error=$lname_error=$mname_error="";

        $username = $_POST['username'];
        $password = $_POST['password'];

		//Firstname
		if(empty($_POST["fname"])){
			$fname_error = "Firstname is required.";
		}else{
			$fname = input_data($_POST['fname']);
			//validate inputted firstname
			if(!preg_match("/^[a-zA-z]*$/", $fname)){
				$fname_error = "Firstname is not in valid format, can only contain letters.";
			}
		}
		//Middlename
		if(!empty($_POST["mname"])){
			$mname = input_data($_POST['mname']);
			if(!preg_match("/^[a-zA-z]*$/", $mname)){
				$mname_error = "Middlename is not in valid format, can only contain letters.";
			}
		}
		//Lastname
		if(empty($_POST["lname"])){
			$lname_error = "Lastname is required.";
		}else{
			$lname = input_data($_POST['lname']);
			if(!preg_match("/^[a-zA-z]*$/", $lname)){
				$lname_error = "Lastname is not in valid format, can only contain letters.";
			}
		}
		//Contact Number
		if(empty($_POST["contact"])){
			$contact_error = "Contact number is required.";
		}else{
			$contact = input_data($_POST['contact']);
			if(!preg_match('/^[0-9]{11}+$/', $contact)){
				$contact_error = "Contact number format is not valid.";
			}
		}
		//Email Address
		if(empty($_POST["email"])){
			$email_error = "Email Address is required.";
		}else{
			$email = input_data($_POST['email']);
			if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
				$email_error = "Email Address format is not valid.";
			}
		}

       
          $sql = mysqli_query($dbc,"INSERT INTO `user`(`user_ID`, `firstname`, `middlename`, `lastname`, `contact_number`, `email_address`, `username`, `password`, `profile_photo`)
           VALUES ('','$fname','$mname','$lname','$contact','$email','$username','$password','')");

            if($sql) {
                echo "<script>Data stored successfully.</script>";
            } else {
                    echo "ERROR!";
                }
        

    
}
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <title>User Information</title>
  </head>
  <body>
    <div class="container">
    <div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-6">
	    <hr>
	    <center><label>USER INFORMATION</label></center>
		    	<form action="" method="POST" role="form" enctype="multipart/form-data">
				<div class="panel panel-default panel-body col-sm-12">
					<div class="form-group hidden">
						<label for="">Firstname</label>
						<input type="text" class="form-control" id="" placeholder="Firstname" name="fname" value="">
                        <span style="color:red"><?= $fname_error; ?></span>
					</div>
					<div class="form-group hidden">
						<label for="">Middlename</label>
						<input type="text" class="form-control" id="" placeholder="Middlename" name="mname" value="">
					</div>
					<div class="form-group hidden">
						<label for="">Lastname</label>
						<input type="text" class="form-control" id="" placeholder="Lastname" name="lname" value="">
                        <span style="color:red"><?= $lname_error; ?></span>
					</div>
					<div class="form-group hidden">
						<label for="">Contact Number</label>
						<input type="text" class="form-control" id="" placeholder="Contact Number" name="contact" value="">
                        <span style="color:red"><?= $contact_error; ?></span>
					</div>
					<div class="form-group hidden">
						<label for="">Email Address</label>
						<input type="text" class="form-control" id="" placeholder="Email Address" name="email" value="">
                        <span style="color:red"><?= $email_error; ?></span>
					</div>
                    <div class="form-group hidden">
						<label for="">Username</label>
						<input type="text" class="form-control" id="" placeholder="Username" name="username" value="">
					</div>
					<div class="form-group hidden">
						<label for="">Password</label>
						<input type="password" class="form-control" id="" placeholder="Password" name="password" value="">
					</div><br>
					<div class="form-group hidden">
						<center><button class="btn btn-primary" name="submit">Submit</button></center>
					</div>
				</div>
			</form>
    		</div>
    </div>
	</div>
  </body>
</html>

