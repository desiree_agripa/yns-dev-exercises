<?php 
include_once 'connect.php';  
$sql = mysqli_query($dbc,"SELECT * FROM `user`");
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <title>Listing User Information</title>
  </head>
  <body>
    <div class="container">
    <div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-6">
	    <hr>
	    <center><label>USER INFORMATION</label></center> <hr>
		    	<form action="" method="POST" role="form" enctype="multipart/form-data">
				<div class="panel panel-default panel-body col-sm-12">
					<table class="table table-condensed table-bordered text-center">
						<thead>
							<th>User ID</th>
							<th>Firstname</th>
							<th>Middlename</th>
							<th>Lastname</th>
							<th>Contact Number</th>
							<th>Email Address</th>
						</thead>
						<tbody>
							<?php
									while($data=mysqli_fetch_assoc($sql)): ?>
											<tr>
											<td><?= $data['user_ID']; ?></td>
											<td><?= $data['firstname']; ?></td>
											<td><?= $data['middlename']; ?></td>
											<td><?= $data['lastname']; ?></td>
											<td><?= $data['contact_number']; ?></td>
											<td><?= $data['email_address']; ?></td>
										</tr>
									<?php endwhile;

							 ?>
									</tbody>
								</table>
							</div>
						</form>
    		</div>
    	</div>
		</div>
  </body>
</html>

	
