<?php
session_start();
if(isset($_SESSION["login"]) && $_SESSION["login"] === true) {
    header("location: 1-10(1).php");
    exit;
}
require_once "connect.php";
$username = $password = "";
$username_error = $password_error= $login_error= "";

if (isset($_POST['login_btn'])) {
    $usename = $_POST['username'];
    $password = $_POST['user_password'];
    $q = mysqli_query($dbc,"SELECT * FROM user WHERE (username='$username') AND password='$password'");
    $count=mysqli_num_rows($q);
    if ($count==1) {
        $_SESSION['login']=$username;
        $msg = "Logging...";
        $sts ="success";
        redirect('1-10(1).php',2000);
    }else{
        $msg = "Invalid Username or Password<br> Your might be choosen wrong actor";
        $sts = "danger";
    }
}
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <title>Login</title>
  </head>
  <body>
    <div class="container">
    <div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-6">
	    <hr>
	    <center><label>LOGIN</label></center>
	    <div class="alert text-warning">
          <?php if (isset($error)): ?>
            <?php echo $error ?>
          <?php endif; ?>
        </div>
		    	<form action="" method="POST" role="form" enctype="multipart/form-data">
				<div class="panel panel-default panel-body col-sm-12">
					<div class="form-group hidden">
						<label for="">User ID</label>
						<input type="text" class="form-control" id="" placeholder="User ID" name="user_ID">
						<span style="color:red"><?= $userID_error; ?></span>
					</div>
					<div class="form-group hidden">
						<label for="">Password</label>
						<input type="password" class="form-control" id="" placeholder="Password" name="password">
						<span style="color:red"><?= $password_error; ?></span>
					</div>
					<div class="form-group hidden"><br>
						<center><button class="btn btn-primary" name="login">Login</button></center>
					</div>
				</div>
			</form>
    		</div>
    </div>
	</div>
  </body>
</html>

	
