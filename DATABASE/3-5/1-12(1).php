<?php 
include_once 'connect.php';  
$sql = mysqli_query($dbc,"SELECT COUNT(*) as counter FROM `user`");
while($data=mysqli_fetch_assoc($sql)){
    $all_data = $data['counter'];

//$all_data=$data_in_page=[];

//Getting pagination number
if(!isset($_GET['page'])){
	$page = 1;
}else{
	$page = $_GET['page'];
}

	$number_pages = ceil($all_data/10);
	$data_in_page = $all_data/10;
}
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <title>Display </title>
  </head>
  <body>
    <div class="container">
    <div class="row">
    <div class="col-sm-2"></div>
    <div class="col-sm-9">
	    <hr>
	    <center><label>USER INFORMATION</label></center> <hr>
		    	<form action="" method="POST" role="form" enctype="multipart/form-data">
				<div class="panel panel-default panel-body col-sm-12">
					<table class="table table-condensed table-bordered text-center">
						<thead>
							<th>No.</th>
							<th>Firstname</th>
							<th>Middlename</th>
							<th>Lastname</th>
							<th>Contact Number</th>
							<th>Email Address</th>
							<th>Photo</th>
						</thead>
                        <tbody>
							<?php   $sqlinfo = mysqli_query($dbc,"SELECT * FROM `user`");
									while($datafiles=mysqli_fetch_assoc($sqlinfo)): ?>
											<tr>
											<td><?= $datafiles['user_ID']; ?></td>
											<td><?= $datafiles['firstname']; ?></td>
											<td><?= $datafiles['middlename']; ?></td>
											<td><?= $datafiles['lastname']; ?></td>
											<td><?= $datafiles['contact_number']; ?></td>
											<td><?= $datafiles['email_address']; ?></td>
                                            <td><img src="upload/<?= $datafiles['profile_photo'] ?>" width="100px" height="100px"></td>
										</tr>
									<?php endwhile;

							 ?>
									</tbody>
								</table>
							</div>
								<div class="">
									<label for=""></label><center>
									<?php
											for($pages=1;$pages<=$number_pages;$pages++){ ?>
												<a href="1-12(1).php?page=<?= $pages; ?>"><?= $pages; ?></a>
													<?php	} ?>
										</center>
								</div>
						</form>
    		</div>
    	</div>
		</div>
  </body>
</html>

	
